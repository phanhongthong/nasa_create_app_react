import React, { Fragment } from "react";
import "./Spinner.css";

export default () => (
  <Fragment>
    <div>
      <div class="smt-spinner-circle">
        <div class="smt-spinner"></div>
      </div>
    </div>
  </Fragment>
);
